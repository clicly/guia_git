# -*- org-confirm-babel-evaluate: nil -*-y
#+LATEX_COMPILER: lualatex
#+LANGUAGE: es
#+LATEX_HEADER: \usepackage[spanish]{babel}
#+TITLE:¿Cómo viajar y aprender git a la vez?
#+Author: Raquel Bracho
#+Email: raquelbracho2424@gmail.com
#+STARTUP: inlineimages
#+OPTIONS: ^:nil
#+LATEX_HEADER: \usepackage{fancyhdr}
#+LATEX_HEADER: \pagestyle{fancyplain}
#+LATEX_HEADER: \usepackage{svg}
#+LATES_HEADER: \spanishdatefirst
#+LATEX_HEADER: \lhead{}
#+LATEX_HEADER: \rhead{}
#+LATEX_HEADER: \cfoot{Introducción a git | Raquel Bracho}
#+OPTIONS: H:2 toc:1 num:f author:t date:f email:t
#+LATEX_HEADER: \usepackage{tabularx}
#+latex_header: \usepackage{fontawesome}
#+STARTUP: latexpreview

* P A R T E  1: definiciones
* ¿Qué es git?

Hace mucho tiempo he querido viajar, me tomé la tarea de investigar sobre 
herramientas que faciliten la organización de mi viaje, no conforme con 
eso tambíen debería aprender algo en el proceso para mi área laboral, me
gusta la programación y esta es una oportunidad perfecta. Viendo, preguntando,
cierta persona me habló de Git, un Sistema de Control de Versiones, /(Version/
/Control System/) *VSC*, éste sistema registra los cambios realizados en un 
archivo o conjunto de archivos a lo largo del tiempo.
Es una excelente opción:
- Es veloz
- Su diseño es sencillo
- Tiene soporte para desarrollo no lineal, pueden haber miles de ramas paralelas
- Una capacidad estupenda para manejar grandes proyectos.

Y ahora se convertirá en el protagonista de esta historia de aprendizaje.

** Manejo de datos
Git maneja los datos como un conjunto de /copias instanteneas/ de un sistema de 
archivos miniatura. Al /confirmar/ un cambio git genera una copia de los
archivos procesados hasta ese momento, guarda una referencia a esa copia
instantenea. Podemos comparar las siguientes figuras y observar un cambio en el
flujo de trabajo a traves del tiempo.

Esta figura hace referencia a otro tipo de VCS que no es git
#+CAPTION: Almacenamiento de datos como cambios en una versión de la base de cada
archivo.
#+ATTR_HTML: :width 300
#+ATTR_ORG: :width 300
#+ATTR_LATEX: width 300
[[file:./img/no_git.png]]

Este VCS se caracteriza por almacenar las copias pero no lo hace sobre alguna copia
anterior sino que con el paso del tiempo va dejando en algún lugar la última copia
que se subió en ese entonces, es decir, no reemplaza sino que pueden haber cientos
archivos de una misma versión, modificados.

#+CAPTION: Almacenamiento de copias instantáneas del proyecto a través del
tiempo
#+ATTR_HTML: :width 300
#+ATTR_ORG: :width 300
#+ATTR_LATEX: width 300
[[file:./img/si_git.png]]

* Checksum
Es la verificación de datos mediante una /suma de comprobación/. Esto sucede
justo antes de que el  dato sea almacenado, se identifica mediante ese checksum.
Este mecanismo se conoce como *hash SHA-1*, no es un estornudo,  Es una cadena 
hexadecimal compuesta de 40 caracteres y se calcula en base a los contenidos el 
archivo o estructura del directorio en git. Así se ve un hash:

/4b9da6552252987aa493b52f8696cd6d3b00373/

*** ¿Qúe se supone que eso de hexadecimal?
Es un sistema de numeración posicional que usa como base el número *16*, ocupa
caracteres del alfabeto, *A, B, C, D, E* y *F* y números naturales *(0-9)*.
Esto es para reducir la cantidad de grandes cadenas de números binarios que sólo
entienden los computadores. Git 'transforma' resultados binarios en unos más
entendibles para nosotros los seres humanos.

* Las 3 secciones principales de un proyecto en git

#+CAPTION: 3 secciones principales
#+ATTR_HTML: :width 300px
#+ATTR_ORG: :width 300px
#+ATTR_LATEX: :width 200px
[[file:./img/sections.png]]

** Git directory
*DIRECTORIO DE GIT*

Es donde se almacena la base de datos de objetos para el proyecto. Esto es lo 
que se copia cuando se /clona un repo/. 
 
** Working directory
*DIRECTORIO DE TRABAJO*

Es una copia de una versión del protecto, esos archivos se "sacan" desde la base
de datos comprimida en el directorio de git y se colocan en el disco para poder
modificarlos.
 
** Staging area

*ÁREA DE PREPARACIÓN*

Es un archivo contenido en /"mi directorio git"/ que almacena información acerca
de lo que va en la próxima confirmación.

* Los 3 estados principales en un proyecto git
** commited
*CONFIRMADO*

significa que los datos están almacenados de manera segura en la base de datos 
local.

** modified
*MODIFICADO*

El archivo se modificó pero todavia no ha habido /confirmación/ a la base de
datos.

** staged
*PREPARADO*

Se ha marcado un archivo modificado en su versión actual para que vaya a la 
próxima confirmación.

#+CAPTION: 3 estados principales en un proyecto git
#+ATTR_HTML: :width 300px
#+ATTR_ORG: :width 300px
#+ATTR_LATEX: :width 100px
[[file:./img/estados.png]]

* Flujo básico en git
** Los verbos
1. *Modificar* una serie de archivos en mi directorio de trabajo
2. *Preparar* los archivos añadiendolos /(add)/  a mi área de preparación
3. *Confirmar* /(commit)/ los cambios, esto es lo que toma los archivos tal y
   como están en el /staging area/ y almacena esa /copia instantanea/ de manera
permanente en mi /git directory/.

Modifico con un editor de texto, agrego, elimino, etc.

Así preparo los archivos para ser añadidos

#+BEGIN_SRC bash
git add .
#+END_SRC 

Así confirmo los cambios y lo que subiré a git

#+BEGIN_SRC bash
git commit -m "subiendo cambios"
#+END_SRC 

Por ejemplo, si hago git clone [url] de algún repositorio esta será una versión
con un número *hash*. Estando en el directorio git se le considera confirmada
/commited/. Si ha sufrido cambios desde que se obtuvo el repo pero ha sido 
añadida al /staging area/ está preparada//staged/, y si ha sufrido cambios
desde que se obtuvo el repositorio pero *NO* se ha preparado está /modified/
modificada.

#+CAPTION: Ciclo de vida del estado de los archivos
#+ATTR_HTML: :width 300px
#+ATTR_ORG: :width 300px
#+ATTR_LATEX: width 300px
[[file:./img/flujo.png]]

* P A R T E  2: los comandos
* Un boleto: la línea de comandos
Ahora bien, después de haber leído y conocido definiciones nuevas que nos
ayudarán a entender mejor, podemos seguir con nuestro viaje.
Lo primero que debemos tener para viajar es un boleto o un pasaje, yo 
prefiero boleto de avión porque se ve hermoso el cielo. Para otra oportunidad
viajaremos por tierra.

Nuestro boleto para volar será este comando, con el podremos despegar sin más.
** Instalar git (GnuLinux)
#+BEGIN_SRC bash
# Actualizamos 
sudo apt-get update

# Instalamos
sudo apt-get install git

# Verificar versión
git version 

#Configurar nombre de usuario y correo
git config --global user.name "Raquel Bracho"
git config --global user.email "raquelbracho2424@gmail.com"

# Comprobar configuración, mostrará los detalles de todas las configuraciones
# hechas
git config --list
#+END_SRC 

Ya compramos nuestro boleto, tiene nombre, apellido, la versión actual.

Necesito saber algo especifico sobre este comando ¿cómo lo hago?
*** Ayuda de git 
#+BEGIN_SRC bash
# git help <verb>
# Por ejemplo,

git help config

# Mostrará el manual 'config' de git
#+END_SRC
Este comando nos permitirá acceder a todas las opciones disponibles con 
descripciones y explicaciones.

#+ATTR_HTML: :width 300px
#+ATTR_ORG: :width 300px
#+ATTR_LATEX: :width 300px 
[[file:./img/config.png]]

* El viaje: ¿Cómo inicializar un repositorio?
** Si hay un proyecto que queremos subir a git
*** Inicializando un repositorio en un directorio existente
Esto creará un subdirectorio nuevo *.git* contiene todos los archivos necesarios
del repositorio pero todavía no hay nada.

Este pequeño pero muy importante directorio es nuestro 'boleto', sin el no podremos
subir el proyecto al sistema.
Está oculto por defecto, podemos verlo pulsando /*para mostrar el archivo oculto -> Ctrl + H/

Para ir organizandonos crearemos el directorio 'viajes
'
#+BEGIN_SRC bash
$ mkdir viaje

$ git init
Inicializado repositorio Git vacío en /home/raquel/Documentos/viaje/.git/
#+END_SRC 

#+ATTR_HTML: :width 300px
#+ATTR_ORG: :width 300px
#+ATTR_LATEX: :width 50px 
[[file:./img/gt.png]]

Vemos lo que contiene mi directorio .git 
#+BEGIN_SRC bash
$ tree -a
.
└── .git
    ├── branches
    ├── config
    ├── description
    ├── HEAD
    ├── hooks
    │   ├── applypatch-msg.sample
    │   ├── commit-msg.sample
    │   ├── fsmonitor-watchman.sample
    │   ├── post-update.sample
    │   ├── pre-applypatch.sample
    │   ├── pre-commit.sample
    │   ├── prepare-commit-msg.sample
    │   ├── pre-push.sample
    │   ├── pre-rebase.sample
    │   ├── pre-receive.sample
    │   └── update.sample
    ├── info
    │   └── exclude
    ├── objects
    │   ├── info
    │   └── pack
    └── refs
        ├── heads
        └── tags

10 directories, 15 files
#+END_SRC 

* ¿Cómo clonar un repositorio?
Con git clone podemos copiar el código de un repositorio de forma local, 
pero no estará conectado al repositorio de origen.

** Cada archivo puede tener 2 estados
*** Tracked files/Archivos Rastreados
Son todos los que estaban en la última copia del proyecto (útimo commit).
pueden ser:
+Sin modificar
+Modificados
+Preparados

/Se refiere a los que no entraron en el último commit que se hizo./

*** Sin reastrear
Son los que *NO* están en la última instantanea y que *NO* están en el área
de preparación.

*** Detalles de un repositorio

**** Clonado por primera vez:
+ Todos los archivos están rastreados
+ Sin modificar
+ Sin editar

**** Editando el archivo
+ Modificado/modified
+ Preparado/staged
+ Confirmado/confirmado

** Paso 1: Copiar y pegar la url
*** git clone [url] <directorio>

#+BEGIN_SRC bash
# El comando general es -> git clone [url]
# Si queremos clonarlo y enviarlo a un directorio al mismo tiempo
# git clone [url] <directorio>

git clone https://gitlab.com/Raquelbracho77/ejemplo mi_ejemplo
#+END_SRC

#+ATTR_HTML: :width 300px
#+ATTR_ORG: :width 300px
#+ATTR_LATEX: width 300px 
[[file:./img/1.png]]

* Trabajando dentro del repositorio 
1. Si el repo existe lo inicializamos
2. Creamos el proyecto en la plataforma git

Si no existe:
1. Creamos una carpeta
2. Comenzamos a trabajar dentro
** Paso 2: Modificar y editar 
*** Editores, editores everywhere
-nano
-vim
-emacs

Todo depende de lo que necesitemos hacer, podemos editar un archivo con
cualquier editor de texto, según la complejidad del 'problema' que queramos
resolver.
Como este es nuestro repo de viajes, crearemos archivos desde cero, asumiendo
que los archivos, carpetas y demás serán los elementos que nos acompañaran a
lo largo del viaje.

#+BEGIN_SRC bash
touch dinero.txt maleta.txt ropa.txt
# Vemos que hay 
$ tree
.
├── dinero.txt
├── maleta.txt
└── ropa.txt

0 directories, 3 files
#+END_SRC

*** git status
Permite revisar con detalle el estado del directorio inicializado, no hay
commits todavía.

#+BEGIN_SRC bash
$ git status
En la rama master

No hay commits todavía

Archivos sin seguimiento:
  (usa "git add <archivo>..." para incluirlo a lo que se será confirmado)

	dinero.txt
	maleta.txt
	ropa.txt

no hay nada agregado al commit pero hay archivos sin seguimiento presentes 
(usa "git add" para hacerles seguimiento)
#+END_SRC

*** Opción detallada de git status
/Crearemos nuevos archivos y seguiremos editando los que ya existen/
/adicionalmente agregaremos una nueva opción a/ *git status* /para ver más/
/detalle del proceso/:

#+BEGIN_SRC bash
$ touch celular
#+END_SRC 

Hay algo diferente en el nuevo estado...

#+BEGIN_SRC bash
git status -s
?? celular.txt
?? dinero.txt
?? maleta.txt
?? ropa.txt
#+END_SRC

 Archivos sin seguimiento:
*??* Archivos nuevos sin ser rastrados

** Paso 3: Agregar los elementos nuevos y modificados
*** git add .

Traemos 4 archivos *sin seguimiento* (nuevos)

#+BEGIN_SRC bash
$ git add .
#+END_SRC


#+BEGIN_SRC bash
 git status -s
A  celular.txt
A  dinero.txt
A  maleta.txt
A  ropa.txt
#+END_SRC 

*A* del inglés Added.

Olvidamos unas chanclas por si pasamos a la playa, podemos ponerlas
dentro de la maleta:
Y vemos el estado de los nuevos archivos
#+BEGIN_SRC bash
$ echo "chanclas" > maleta.txt 
$ cat maleta.txt 
chanclas
$ git status -s
A  celular.txt
A  dinero.txt
AM maleta.txt
A  ropa.txt
#+END_SRC

El archivo fue (M) modificado pero aún no ha sido rastreada esta nueva
edición.

*** Agregar archivo especifico
Para esto ocuparemos el comando git add acompañado del archivo especifico.

#+BEGIN_SRC bash
$ git add maleta.txt 
#+END_SRC

** Paso 4: ¡Hacer el primer commit!
*** git commit -m "un comentario"

#+BEGIN_SRC bas
$ git commit -m "Organizando mi viaje"
[master (commit-raíz) fb19b24] Organizando mi viaje
 4 files changed, 2 insertions(+)
 create mode 100644 celular.txt
 create mode 100644 dinero.txt
 create mode 100644 maleta.txt
 create mode 100644 ropa.txt
#+END_SRC 

*** Dentro de los detalles del commit veremos:
/contenido en los corchetes/:
+ *master*, es la rama en la que estamos ubicados dentro del proyecto.

+ /valor hash hexadecimal, no todos son iguales/ *fb19b24* es un pequeño segmento
de la composición de los elementos que estan dentro del proyecto.

+ *comentario*

+ En este caso *4 files changed, 1 insertion*, fueron los archivos modificados y
  cambiados

+ *create mode*, viene siendo la forma en que git genera y guarda los metadatos 

** Paso 5: Configurar la transmisión del proyecto
*** git push -u origin master

Subiendo el proyecto. Como ya tenemos un directorio local inicializado debemos
agregar la *url* del proyecto vacío en git.

Por seguridad, si dejamos algún archivo suelto es mejor ir agregándolos como en
el *Paso 3*

#+BEGIN_SRC bash
$ git remote add origin git@gitlab.com:Raquelbracho77/viajes.git
#+END_SRC

Luego hacer 'push':
#+BEGIN_SRC bash
git push -u origin master
#+END_SRC 

Este comando se utilizará para darle el toque final a nuestro trabajo y
'publicar' lo que hemos hecho.
En otras palabras se podría describir como que le damos una orden a git para que
actualice la rama que le indicamos. Adicionalmente con la opción *-u*, el manual 
*push* hace referencia a esta opción como /--set-upstream/.

- /Git, empuja mi proyecto hasta la rama de origen/
#+BEGIN_SRC bash
-u, --set-upstream
           For every branch that is up to date or successfully pushed, add
           upstream (tracking) reference, used by argument-less git-pull(1)
           and other commands. For more information, see
           branch.<name>.merge in git-config(1).
#+END_SRC 


Revisamos nuevamente el estado del directorio
#+BEGIN_SRC bash
$ git status 
En la rama master
Tu rama está actualizada con 'origin/master'.

nada para hacer commit, el árbol de trabajo está limpio
#+END_SRC

*Hemos hecho nuestro primer commit y push.

/Acá está la lista de comandos a usar nuevamente si tenemos un directorio/
/existente:/

#+BEGIN_SRC bash
$ cd existing_folder
$ git init
$ git remote add origin git@gitlab.com:[url_del_proyecto]
$ git add .
$ git commit -m "Comentario breve"
$ git push -u origin master
$ git status
#+END_SRC

* Ignorar elementos en git
** .gitignore
Es un archivo 'oculto' que se da la tarea de mantener en nuestro disco local
sin subir los archivos que este contenga.
Para este ejemplo crearemos otro archivo, una lista de lo que contiene el
neceser y un paraguas y un archivo que será oculto:
- *.gitignore*

#+BEGIN_SRC bash
$ touch neceser.md paraguas .gitignore
$ nano neceser.md
#+END_SRC 

Agregamos el contenido

#+BEGIN_SRC bash
#----editor------
- [x] cepillo de dientes
- [x] crema dental
- [x] hilo dental
- [x] desodorante
- [x] Loción humectante
- []
- []
- []
- []
- []
- []
#+END_SRC 

Vamos a una zona fría sin lluvia, no necesitaremos el paraguas, ocupamos
.gitignore:

#+BEGIN_SRC bash
$ nano .gitignore

# dejamos el paraguas en casa
paraguas
#+END_SRC

*Al subir el proyecto nuevamente este archivo se encargará de 'ignorar'*
*los elementos que le indiquemos, incluyéndolos dentro del mísmo*.

* README
Hasta este punto hemos aprendido lo básico de git, algunas definiciones
importantes, un listado de comandos que nos será muy util memorizar, como
clonar, editar y subir nuestro proyecto.
* P A R T E  3: más comandos importantes en git
* Eliminar archivos
#+BEGIN_SRC bash
$ rm archivo.txt
#+END_SRC 

* Mostrar el historial de los commits
#+BEGIN_SRC bash
$ git log 
#+END_SRC 

** Opciones de gitlog
#+BEGIN_SRC bash
# Muestra los dos últimos commit hechos
$ git log -p -2

# Muestra una lista con detalles de todos los archivos modificados por cada
# confirmación.
$ git log --stat 
#+END_SRC 

* format en git
** git log pretty=oneline
Muestra los commits en una sola línea

*** git log pretty=format... 

Es importante conocer este comando a la hora de gestionar un proyecto grande.

#+BEGIN_SRC bash
# Especifica un formato personalizado
$ git log --pretty=format:"%h - %an, %ar : %s"
b88f6bf - Raquel Bracho, hace 20 minutos : nuevos cambios 2
f2cf16a - Raquel Bracho, hace 34 minutos : nuevos cambios
f64e345 - Raquel Bracho, hace 5 horas : Primer commit en el proyecto
ddc6b33 - Raquel Bracho, hace 2 días : Subiendo ejemplos para guía
#+END_SRC

* PARTE  4: Ramas, merge, fork
* Ramas/branches
Las ramas son creadas con el objetivo de desarrollar funcionalidades aisladas 
unas de otras. Una rama nueva no estará disponible para los demás a menos que
se suba (push).
La rama master es la rama "por defecto" cuando se crea  un nuevo repositorio.
Crear nuevas ramas durante el desarrollo y fusiónalarlas a la rama principal
cuando terminemos es recomendable.

** Crear ramas

#+CAPTION: Creación de rama
#+ATTR_HTML: :width 300
#+ATTR_ORG: :width 300
#+ATTR_LATEX: width 300
[[file:./img/rama.png]]

Nuestro primer destino dijimos que era templado, poco lluvioso, Japón.
Esta es nuestra rama master, será al primer lugar al que iremos.
Luego quiero ir a un lugar donde llueva muchisimo porque me gusta la 
lluvia, Agumbe, India.

Crearemos una rama y con la opción -b pasaremos a ella para gestionarla:

#+BEGIN_SRC bash
# Permite ver en que rama estamos
$ git branch
*master

#Creamos nueva rama
$ git checkout -b agumbe
Cambiado a nueva rama 'agumbe'

# Comprobamos...
$ git branch
*agumbe
  master

#+END_SRC 

** Moverse entre las ramas
Es el mismo proceso, escribimos git checkout, adicional el nombre de la
rama.
#+BEGIN_SRC bash
$ git checkcout master
#+END_SRC 

*Por ahora trabajaremos en la rama agumbe*

** Trabajando en la nueva rama
Recordamos que cada commit que realizamos es una nueva versión de una
copia.

Siguiendo con el viaje, mi nacionalidad no me permite ir a Agumbe, India
sin visa, necesito procesar una [[https://es.ivisa.com/india-visa-application?nationality=VE][ivisa]]
Este proceso lo realizaremos dentro de la rama *agumbe*:

#+BEGIN_SRC bash
$ touch formulario_agumbe.txt

# Sellado del pasaporte para ivisa
$ touch pasaporte.txt
$ $ echo 'Turista por 30 días' >pasaporte.txt

# Compramos el boleto
touch boleto_agumbe ivisa_agumbe.txt
#+END_SRC 

Nuevamente comprobamos el proceso hasta ahora:

#+BEGIN_SRC bash
$ git commit
En la rama agumbe
Archivos sin seguimiento:
	.gitignore
	boleto_agumbe
	formulario_agumbe.txt
	ivisa_agumbe.txt
	neceser.md
	pasaporte.txt

no hay nada agregado para confirmar, pero hay archivos sin seguimiento presentes
#+END_SRC 

- Hicimos el commit mucho antes de aprender a ignorar los archivos
- *.gitignore* quedó agregado, listo para el próximo commit, falta darle
seguimiento
- Creamos la rama *agumbe*
- Creamos nuevos archivos
- utilizamos el comando /git commit/ sin opciones para verificar el
'status' de la rama.

+ *NOTA:* /git commit/ sin opciones, solo se puede hacer si no se ha
utilizado el comando /git add . / ántes, sino dará un mensaje en blanco.

#+BEGIN_SRC bash
$ git add .
$ git status
En la rama agumbe
Cambios a ser confirmados:
  (usa "git reset HEAD <archivo>..." para sacar del área de stage)

	nuevo archivo:  .gitignore
	nuevo archivo:  boleto_agumbe
	nuevo archivo:  formulario_agumbe.txt
	nuevo archivo:  ivisa_agumbe.txt
	nuevo archivo:  neceser.md
	nuevo archivo:  pasaporte.txt
#+END_SRC 

** REPASO: Comandos utiles para gestionar ramas
#+BEGIN_SRC bash
# Verificar la lista de ramas disponibles y la rama activa actual 
git branch

# Crear una nueva rama
git branch nombre_de_la_rama

# Desarrollar sobre una nueva rama 
git checkout nombre_de_la_rama

# Crear una nueva rama y cambiar la rama activa 
git checkout -b nombre_de_la_rama

# Cambiar el nombre de la rama actual 
git branch -m nuevo_nombre_de_la_rama


# Eliminar una rama 
git branch -D nombre_de_la_rama

#+END_SRC

** Caso 1: 
Estoy en Agumbe,año sabático,  agilicé el papeleo y conseguí 
trabajo de chef de medio tiempo en un restaurant en el centro. 
No se nada de comida india, tengo que organizar todo mi trabajo, 
para ello crearé una nueva rama 'recetas':

#+BEGIN_SRC bash
# crear rama
$ git checkout -b recetas

# Crear carpeta
$ mkdir recetas_agumbe

# Guardar recetas
$ cd recetas_agumbe/
$ $ touch Samosa Masala_Dosa kati_roll

/recetas_agumbe$ tree
.
├── kati_roll
├── Masala_Dosa
└── Samosa

0 directories, 3 files

# Salimos de la carpeta nueva y verificamos
$ cd ..
/viaje$ git status

$ git add .

En la rama recetas
Cambios a ser confirmados:
  (usa "git reset HEAD <archivo>..." para sacar del área de stage)

	nuevo archivo:  recetas_agumbe/Masala_Dosa
	nuevo archivo:  recetas_agumbe/Samosa
	nuevo archivo:  recetas_agumbe/kati_roll

# Confirmar los cambios
$ git commit -m "Subiendo recetas en agumbe"
[recetas cb8b2cb] Subiendo recetas en agumbe
 3 files changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 recetas_agumbe/Masala_Dosa
 create mode 100644 recetas_agumbe/Samosa
 create mode 100644 recetas_agumbe/kati_roll

# Push, debe hacerse justo en la rama que creamos para esta función
$ git push -u origin recetas

# LISTO!
#+END_SRC

** Caso 2:
Pasó un tiempo desde que postulé a un trabajo dentro de una agencia
de publicidad como redactora, necesito llevar un orden en lo que hago
y casualmente me ha tocado escribir un articulo de 'lugares y comidas
típicas' en Agumbe, he decidido crear otra rama:

#+BEGIN_SRC bash
# crear rama
$ git checkout -b lugares

# Crear otra carpeta 
$ mkdir lugares_agumbe

# Chequear contneido de la carpeta
tree
.
├── barkana_falls.jpg
└── karnataka.jpg

0 directories, 2 files

# Ver el estado de los nuevos archivos y directorio
$ git status -s
?? lugares_agumbe/

# Agregar
$ git add .

# Revisar nuevamente que hayan sido agregados
$ git status -s 
A  lugares_agumbe/barkana_falls.jpg
A  lugares_agumbe/karnataka.jpg

# Hacer commit
$ git commit -m "subiendo fotos de lugares, agumbae"
[lugares 9855cdb] subiendo fotos de lugares, agumbae
 2 files changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 lugares_agumbe/barkana_falls.jpg
 create mode 100644 lugares_agumbe/karnataka.jpg

# Push
$ git push -u origin lugares 
Contando objetos: 5, listo.
Delta compression using up to 8 threads.
Comprimiendo objetos: 100% (5/5), listo.
Escribiendo objetos: 100% (5/5), 233.92 KiB | 5.85 MiB/s, listo.
Total 5 (delta 1), reused 0 (delta 0)
remote: 
remote: To create a merge request for lugares, visit:
remote:   https://gitlab.com/Raquelbracho77/viajes/-/merge_requests/new?merge_request%5Bsource_branch%5D=lugares
remote: 
To gitlab.com:Raquelbracho77/viajes.git
 * [new branch]      lugares -> lugares
Rama 'lugares' configurada para hacer seguimiento a la rama remota 'lugares' de 'origin'.
#+END_SRC

* Fusión: merge
** Unir ramas

#+CAPTION: merge
#+ATTR_HTML: :width 300
#+ATTR_ORG: :width 300
#+ATTR_LATEX: width 300
[[file:./img/merge.png]]

Al igual que /en dragon ball, Gokú se fusiona con Gohan/,
git, tiene una 'técnica', una opción que permite fusionar o
unir dos o más ramas. Esto debe hacerse dentro de la rama que
está activa, es decir, dentro de la rama en la que nos
encontramos:

#+BEGIN_SRC bash
# Rama activa
$ git branch
  agumbe
*lugares
  master
  recetas
#+END_SRC

- *Fusionaremos* las ramas *lugares* con *recetas*.

#+BEGIN_SRC bash
$ git merge recetas 
Ya está actualizado.
#+END_SRC

- Además verificamos en que rama estamos:

#+BEGIN_SRC bash
$ git branch
  agumbe
*lugares
  master
  recetas

#lista ramas que han sido mezcladas con la actual.
$ git branch --merged
  agumbe
 *lugares
  master
  recetas
#+END_SRC

- Actualizamos, agregamos y subimos los cambios al repositorio

#+BEGIN_SRC bash
$ git add .

$ git commit -m "Fusión de rama lugares con recetas"

$ git push -u origin lugares
Rama 'lugares' configurada para hacer seguimiento a la rama remota
 'lugares' de 'origin'.
Everything up-to-date

#+END_SRC

- Para finalizar, fusionaremos nuevamente, esta vez la rama
*lugares* con *agumbe*
 
#+BEGIN_SRC bash
$ git checkout agumbe 
Cambiado a rama 'agumbe'
Tu rama está actualizada con 'origin/agumbe'.

$ git branch
*agumbe
  lugares
  master
  recetas
#+END_SRC

- En la terminal, se ve así el proceso de fusión
#+BEGIN_SRC bash
$ git merge lugares 
Actualizando 98572ea..9855cdb
Fast-forward
 lugares_agumbe/barkana_falls.jpg | Bin 0 -> 75063 bytes
 lugares_agumbe/karnataka.jpg     | Bin 0 -> 164759 bytes
 recetas_agumbe/Masala_Dosa       |   0
 recetas_agumbe/Samosa            |   0
 recetas_agumbe/kati_roll         |   0
 5 files changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 lugares_agumbe/barkana_falls.jpg
 create mode 100644 lugares_agumbe/karnataka.jpg
 create mode 100644 recetas_agumbe/Masala_Dosa
 create mode 100644 recetas_agumbe/Samosa
 create mode 100644 recetas_agumbe/kati_roll
#+END_SRC

- Como no hemos agregado nada sino que solo unimos las ramas,
no usaremos git add .

*NOTA*:
/Asegurarnos siempre de que no haya ningun archivo fuera del
area de preparación sin seguimiento ni ser confirmado./

#+BEGIN_SRC bash
# Commit
$ git commit -m "Fusión de rama lugares con agumbe"
En la rama agumbe
Tu rama está adelantada a 'origin/agumbe' por 2 commits.
  (usa "git push" para publicar tus commits locales)

nada para hacer commit, el árbol de trabajo está limpio

# Push
$ git push -u origin agumbe
Total 0 (delta 0), reused 0 (delta 0)
remote: 
remote: To create a merge request for agumbe, visit:
remote:   https://gitlab.com/Raquelbracho77/viajes/-/merge_requests/new?merge_request%5Bsource_branch%5D=agumbe
remote: 
To gitlab.com:Raquelbracho77/viajes.git

#+END_SRC

Terminada nuestra aventura en Agumbe, sus lugares y comida.

* Fork: 
Con *[[https://git-scm.com/book/en/v2/GitHub-Contributing-to-a-Project][fork]]* podremos crear una copia en remoto de un repositorio git
en nuestra cuenta de GitHub y de esa forma sera mas fácil clonar 
de forma local para hacer pull y push de los cambios realizados.

*Esto no es lo mismo que hacer 'git clone'*

Así es como funciona generalmente:

1. [[https://styde.net/clone-y-fork-con-git-y-github/][Bifurca]] el proyecto.

2. Crear una nueva rama desde master.

2. Comprométete a mejorar el proyecto.

3. Envía esta rama a tu proyecto de Gitlab

4. Abre una solicitud de extracción en Gitlab.

5. Discute y, opcionalmente, continúa comprometiéndote.

6. El propietario del proyecto fusiona o cierra la solicitud de extracción.

7. Sincroniza la rama *master* actualizado con tu fork

#+CAPTION: Solicitar un fork
#+ATTR_HTML: :width 300px
#+ATTR_ORG: :width 300px
#+ATTR_LATEX: :width 300px
[[file:./img/fork.png]]

* Pull request: Realizar una solicitud de incorporación de cambios
[[https://git-scm.com/docs/git-request-pull][git request pull]]
TODO: 
* Jerga git
- Repo: repositorio
- Hacer commit: confirmar los cambios
- Hacer push: subir proyecto
- merge: unir ramas
- fork: contribuir a un proyecto
* Referencias 

- *Libro:Pro Git* *Autores: Scott Chacon, Ben Straub**2nd Edition (2014)*

- Referencias web: 

*IMPORTANTE, LEER:*

[[https://git-scm.com/book/es/v2/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F]]

- Chuleta git:

https://elbauldelprogramador.com/mini-tutorial-y-chuleta-de-comandos-git/#pasos-a-seguir-para-contribuir-a-proyecyos-ajenos-mediante-fork]]

[[https://j2logo.com/comandos-git-principales/]]

[[https://blog.nubecolectiva.com/conceptos-y-tecnicas-de-git-que-debes-saber-antes-de-unirte-a-un-equipo-de-git-parte-1/]]

[[https://rogerdudler.github.io/git-guide/index.es.html]] 

https://git-scm.com/docs/git-request-pull

